// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Route [POST]
	// Create Product
	route.post('/create', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		let data = req.body;
		isAdmin? controller.createProd(data).then(result => res.send(result))
		: res.send(`User Unauthorized to Proceed!`);
	});

// [SECTION] Route [GET]
	// Retrieve All Active Products
	route.get('/active', (req, res) => {
		controller.getActive().then(result => {
			res.send(result);
		});
	});

	// Retrieve All Inactive Products (Admin)
	route.get('/inactive', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		isAdmin ? controller.getInactive().then(outcome => res.send(outcome))
		: res.send(`User Unauthorized to Proceed!`);
	});

	// Retrieve All Products (Admin)
	route.get('/all', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		isAdmin? controller.getAllProd().then(result => res.send(result))
		: res.send(`User Unauthorized to Proceed!`);
	});
	
	// Retrieve Single Product
	route.get('/:id/view', (req, res) => {
		let data = req.params.id;
		controller.getProd(data).then(result => {
			res.send(result);
		});
	});

// [SECTION] Route [PUT]
	// Update Product Information (Admin)
	route.put('/:prodId/update', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params;
		let body = req.body;
		isAdmin ? controller.updateProd(params, body).then(result => res.send(result))
		: res.send(`User Unauthorized to Proceed!`);
	});

	// Product Archive (Admin)
	route.put('/:prodId/archive', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params

		isAdmin ? controller.archiveProd(params).then(result => res.send(result))
		: res.send(`User Unauthorized to Proceed!`);
	});

	// Product Active (Admin)
	route.put('/:prodId/activate', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params

		isAdmin ? controller.activeProd(params).then(result => res.send(result))
		: res.send(`User Unauthorized to Proceed!`);
	});

// [SECTION] Route [DEL]
	// Product Hard Delete
	route.delete('/:prodId/delete', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let params = req.params.prodId;
		isAdmin ? controller.deleteCourse(params).then(deleted => res.send(deleted))
		: res.send(`User Unauthorized to Proceed!`);
	});

// [SECTION] Expose Route System
	module.exports = route;