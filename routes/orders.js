// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/orders');
	const auth = require('../auth');

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Route [POST]
	// Create Order
	route.post('/cart', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let userId = payload.id;
		let isAdmin = payload.isAdmin;
		let pId = req.body.prodId;
		let quantity = req.body.quantity;
		let data = {
			userId: userId,
			prodId: pId,
			quantity: quantity
		};

		if (!isAdmin) {
			controller.createOrder(data).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send(`Admministrator accounts are not allowed to order!`);
		}
	});

// [SECTION] Route [GET]
	// Retrieve All Orders
	route.get('/all', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		isAdmin ? controller.getAllOrders().then(result => res.send(result))
		: res.send(`User Unauthorized to Proceed!`);
	});

	// Retrieve Authenticated User's Orders
	route.get('/user-order', auth.verify,(req, res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
		let userId = payload.id;
		isAdmin ? res.send(`User Unauthorized to Proceed!`)
		: controller.getUserOrd(userId).then(outcome => res.send(outcome))
	});

// [SECTION] Expose Route System
	module.exports = route;