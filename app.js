// [SECTION] Dependencies and Modules
	const exp = require('express');
	const mongo = require('mongoose');
	const dotenv = require('dotenv');
	const cors = require('cors');
	const prodRoutes = require('./routes/products');
	const userRoutes = require('./routes/users');
	const orderRoutes = require('./routes/orders');

// [SECTION] Environment Variables Settings
	dotenv.config();
	const port = process.env.PORT;
	const cred = process.env.CSTRING; 

// [SECTION] Server Setup
	const app = exp();
	app.use(cors());
	app.use(exp.json());
	app.use(exp.urlencoded({extended: true}));

// [SECTION] Database Connection
	mongo.connect(cred);
	const db = mongo.connection;
	db.once('open', () => {
		console.log(`Successfully connected to MongoDB Atlas`)
	});
// [SECTION] Server Routes
	app.use('/users', userRoutes);
	app.use('/products', prodRoutes);
	app.use('/orders', orderRoutes);

// [SECTION] Server Responses
	app.get('/', (req, res) => {
		res.send(`E-Commerce Project Deployed Successfully!`);
	});
	app.listen(port, () => {
   		console.log(`Server is currently running on port ${port}`);
	});