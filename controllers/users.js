// [SECTION] Dependencies and Modules
	const nodemailer = require("nodemailer");
	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const auth = require('../auth');

// [SECTION] Funtionalities [POST]
	// Register User
	module.exports.registerUser = (data) => {
		let fName = data.firstName;
		let lName = data.lastName;
		let mName = data.middleName;
		let email = data.email;
		let passW = data.password;
		let mobNo = data.mobileNo;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			middleName: mName,
			mobileNo: mobNo,
			email: email,
			password: bcrypt.hashSync(passW, 10)
		});

		return User.findOne({email:email}).then(found => {
			if (found) {
				return false;
			} else {
				return newUser.save().then((user, err) => {
					if (user) {
						return user
					} else {
						return false;
					}
				});
			}
		});
	};

	// Login (User Authentication)
	module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email;
		let uPass = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				return false;
			} else {
			let passW = result.password;
				const isMatched = bcrypt.compareSync(uPass, passW);
				if (isMatched) {
					let data = result.toObject();
					return {access: auth.createAccessToken(data)}
				} else {
					return false
				}
			}
		});
	};

// [SECTION] Functionality [GET]
	// Retrieve all Users (Admin)
	module.exports.getAllUsers = () => {
		return User.find({}).then(result => {
			return result;
		});
	};

	// Retrieve User profile
	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};

// [SECTION] Functionalities [PUT]
	// Set User as Admin
	module.exports.setAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return `Successfully updated ${admin.firstName} to Admin!`;
			} else {
				return `Updates failed to implement.....`;
			};
		});
	};

	// Set User as Non-Admin
	module.exports.setNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return `Successfully updated ${user.firstName} to Non-Admin`;
			} else {
				return `Unable to update user information....`;
			};
		});
	};

	// Change Password
	module.exports.updatePass = (reqBody) => {
		let uEmail = reqBody.email;
		let uPass = reqBody.password;
		let nPass = reqBody.newpassword;

		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				return false;
			} else {
			let passW = result.password;
			let isMatched = bcrypt.compareSync(uPass, passW);
				if (isMatched) {
					let dataNiUser = result.toObject();
					let updates = {
						password: bcrypt.hashSync(nPass, 10)
						}
					return User.findByIdAndUpdate(dataNiUser, updates).then((user, err) => {
						if (user) {
							return true
						} else {
							return false;
						};
					});
				} else {
					return false
				};
			};
		});
	};