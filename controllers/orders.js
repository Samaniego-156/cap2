// [SECTION] Dependencies and Modules
	const Order = require('../models/Order');
	const Prod = require('../models/Product');
	const User = require('../models/User');
	const auth = require('../auth');

// [SECTION] Funtionalities [POST]
// Create Order
	module.exports.createOrder = async (data) => {
		let pId = data.prodId;
		let qty = data.quantity;
		let uId = data.userId;

		let cost = await Prod.findById(pId).then(result => result.price * qty);

		let userUp = await User.findById(uId).then(user => {
			user.orders.push({productId: pId});
			return user.save().then((save, err) => {
				if (err) {
					return false;
				} else {
					return true;
				}
			});
		});

		let newOrder = new Order({
			prodId: pId,
			quantity: qty,
			totalAmt: cost,
			userId: uId
		});

		return newOrder.save().then((save, err) => {
			if (err) {
				return `There was an error in processing your order to the database.`;
			} else {
				return save;
			}
		});
	};

// [SECTION] Functionalities [GET]
	// Retrieve ALL Orders
	module.exports.getAllOrders = () => {
		return Order.find({}).then(result => {
			return result;
		});
	};

	// Retrieve Authenticated User's Orders
	module.exports.getUserOrd = (id) => {
		return Order.find({userId: id}).then(order => {
			return order;
		});
	};
