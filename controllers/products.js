// [SECTION] Dependencies and Modules
	const Prod = require('../models/Product');

// [SECTION] Functionalities [POST]
	// Create Product
	module.exports.createProd = (data) => {
		let pName = data.name;
		let pDesc = data.description;
		let pPrice = data.price;

		let newProd = new Prod({
			name: pName,
			description: pDesc,
			price: pPrice
		});

		return newProd.save().then((prod, err) => {
			if (prod) {
				return prod;
			} else {
				return false
			}
		});
	};

// [SECTION] Functionalities [GET]
	// Retrieve ALL Active Products
	module.exports.getActive = () => {
		return Prod.find({isActive: true}).then(result => {
			return result;
		});
	};

	// Retrieve ALL Inactive Products
	module.exports.getInactive = () => {
		return Prod.find({isActive: false}).then(result => {
			return result;
		});
	};

	// Retrieve ALL Products
	module.exports.getAllProd = () => {
		return Prod.find({}).then(result => {
			return result;
		});
	};

	// Retrieve Single Product
	module.exports.getProd = (id) => {
		return Prod.findById(id).then(result => {
			return result;
		});
	};

// [SECTION] Functionalities [PUT]
	// Update Product Details
	module.exports.updateProd = (prod, details) => {
		let pName = details.name;
		let pDesc = details.description;
		let pCost = details.price
		let updatedProd = {
			name: pName,
			description: pDesc,
			price: pCost
		};
		let id = prod.prodId;
		return Prod.findByIdAndUpdate(id, updatedProd).then((updated, err) => {
			if (updated) {
				return true;
			} else {
				return false;
			}
		});
	};

	// Product Archive
	module.exports.archiveProd = (prod) => {
		let id = prod.prodId;
		let updates = {isActive: false};
		return Prod.findByIdAndUpdate(id, updates).then((archived, error) => {
			if (archived) {
				return	archived;
			} else {
				return 	false;
			}
		})
	};

	// Product Active
	module.exports.activeProd = (prod) => {
		let id = prod.prodId;
		let updates = {isActive: true};
		return Prod.findByIdAndUpdate(id, updates).then((active, error) => {
			if (active) {
				return	true;
			} else {
				return 	false;
			}
		})
	};

// [SECTION] Functionality [DEL]
	// Product Hard Delete
	module.exports.deleteCourse = (prodId) => {
		return Prod.findById(prodId).then(prod => {
			if (prod === null) {
				return false;
			} else {
				return prod.remove().then( (removedCourse, err) =>{
					if (err) {
						return false;
					} else {
						return true;
					};
				});
			};
		});
	};