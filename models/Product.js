// [SECTION] Dependencies and Modules
	const mongo = require('mongoose');

// [SECTION] Schema
	const prodSchema = new mongo.Schema({
		name: {
			type: String,
			required: [true, 'Name is Required!']
		},
		description: {
			type: String,
			required: [true, 'Description is Required!']
		},
		price: {
			type: Number,
			required: [true, 'Price is Required!']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	});

// [SECTION] Models
	module.exports = mongo.model("Product", prodSchema);